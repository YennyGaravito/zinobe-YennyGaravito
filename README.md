# Zinobe Backend Test - PHP - RESUELTO

Hola y muchas gracias por darme la oportunidad para realizar la prueba.

## Instrucciones Instalacion

* Se debe copiar los archivos en el httdocs del servidor donde se va a ejecutar la aplicacion.
* Crear la base de datos Haciendo IMPORT o mysql_dump del archivo bd_usuarios.sql
* Ejecutar por url en un explorador web: "http://localhost/index.php" (localhost puede ser reemplzado por la IP del server.
* Seguir las intrucciones de usuario.

### Características generales
	
El aplicativo ha sido desarrollado basandose en funcionamiento back y es totalmente codigo puro(sin extensiones ni adiciones ni copias de codigo. 
El front es totalmente independiente de la logica de ejecucion de las opciones dadas.
El PHP de conexiones de bases de datos es independiente igualmente.
Cada Vista es asignada a un html independiente para poder hacer facilmente cambios.
Es una muestra de codigo y no es en totalidad una muestra de todos los conocimientos.
La experiencia aqui mostrada es sobre solo back, el front de aplicativos como Frameworks es totalmente independiente
y de los cuales, aunque tengo conocimientos no son mi especialidad sin desmeritar que tengo facil aprendizaje de este tipo 
de herramientas.

***¡GRACIAS!***