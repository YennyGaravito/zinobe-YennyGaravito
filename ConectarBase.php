<?php

	$hostC = 'localhost';
	$usrC = 'root';
	$pasC = '';
	$bdC = 'bd_usuarios';
	
	$dblink = mysqli_connect($hostC,$usrC,$pasC,$bdC) or die("Sin conexion a BD");
	
	function EjecutaConsulta($SQLConsulta){
		global $dblink;
		
		$varResultados = array();
		$p=0;
		$dbResult = mysqli_query($dblink,$SQLConsulta) or die ("Consulta sin realizar:" . mysqli_error($dblink));
		$cuentaRows = mysqli_num_rows($dbResult);
		if($cuentaRows == 0){
			return 0;
		}else{
			while ($row = mysqli_fetch_assoc($dbResult)) {
				foreach ($row as $key => $value) {
					$varResultados[$p][$key] = $value;
					
				}
				$p++;
			}
			mysqli_free_result($dbResult);
			return $varResultados;
		}
	}
	
?>